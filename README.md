Тестовое задание ООО "Очень интересно"
============================

Это веб приложение на yii2 создано врамках тестового задания с целью демонстрации возможностей класса, который позволяет реализовывать параллельные вычисления на РНР.


DIRECTORY STRUCTURE
-------------------

      alfimovd/parallel-computing/             содержит классы для паралленьных вычислений


INSTALL
-------------------
	
	git clone git@bitbucket.org:alfimovd/interesnee-test.git

	cd to/project/dir

	composer install
