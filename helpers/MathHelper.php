<?php

namespace app\helpers;

/**
* Math helper for calculation of pi Monte Carlo
*/
class MathHelper
{
	public static function PiMonteCarlo($dotCount, $size = 1000)
	{
		$i = 0; // итератор
		$nCirc = 0; // Точки попавшие в круг

		while ($i <= $dotCount) {
			$x = rand(0, $size);
			$y = rand(0, $size);
			if( pow($y,2) <= ( pow($size,2)-pow($x,2) ) ){
				$nCirc++;
			}
			$i++;
		}

		return $nCirc/$dotCount*4; 
	}
}