<?php

namespace app\helpers;

/**
* Helper for get data from output
*/
class ParallelHelper
{
    /**
     * get data from output
     * @param array $outputs Array outputs of tasks
     * @return false of array formatted outputs
     */
	public static function FormatOutput($outputs)
	{
		$return = false;
        if($outputs) {
            foreach ($outputs as $key => $output) {
                if (preg_match('!<result>(.*)?</result>!is', $output, $matches) > 0) {
                    $return[] = $matches[1];
                }
            }
        }
        return $return;
	}
}