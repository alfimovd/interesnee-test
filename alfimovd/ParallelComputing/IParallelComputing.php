<?php
namespace alfimovd\ParallelComputing;

/**
* Interface for class implements parallel computing
*/
interface IParallelComputing 
{
    /**
     * Start parallel task
     * @param array post parameters for task
     * @return void
     */
    public function StartTask($post);

    /**
     * Get current task output
     * @return false or array tasks output 
     */
    public function GetTasksOutput();

    /**
     * @return boolean
     */
    public function HasActiveTasks();
}