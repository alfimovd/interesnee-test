<?php
namespace alfimovd\ParallelComputing;

use alfimovd\ParallelComputing\IParallelComputing;


/**
* Сlass implements parallel computing on sockets
* 
* @property string $server
* @property string $url
* @property integer $port
* @property integer $conn_timeout
* @property integer $rw_timeout
* @property integer $size_output
* @property array $sockets
* @property string $errstr
* @property string $errno
*/

class ParallelComputing implements IParallelComputing 
{
    public $port = 80;
    public $conn_timeout = 30;
    public $rw_timeout = 86400;
    public $size_output = 512;

    private $server; 
    private $url; 
    
    public $sockets;
    public $errstr;
    public $errno;

    public function __construct($server, $url) 
    {
       $this->server = $server;
       $this->url = $url;
    }

    /**
     * Start parallel tasks
     * @param array $post parameters for task
     * @return boolean
     */
    public function StartTasks($count_task ,$post) 
    {
        $this->sockets = array();
        for ($i = 0; $i < $count_task; $i++) {
           $this->StartTask($post);
        }
        return true;
    }

    /**
     * Start parallel task
     * @param array $post parameters for task
     * @return boolean
     */
    public function StartTask($post) 
    {
        $fp = fsockopen(
            $this->server,
            $this->port, 
            $this->errno, 
            $this->errstr, 
            $this->conn_timeout);

        $_post = Array();
        if (is_array($post)) {
            foreach ($post as $name => $value) {
                $_post[] = $name.'='.urlencode($value);
            }
        }
        $post = implode('&', $_post);

        stream_set_blocking($fp, false);
        stream_set_timeout($fp, $this->rw_timeout);

        fwrite($fp,
                "POST /$this->url HTTP/1.1\r\nHost: $this->server \r\n".
                "Content-Type: application/x-www-form-urlencoded\r\n".
                "Content-Length: ".strlen($post)."\r\n".
                "Connection: Close\r\n\r\n".$post
        );
        $this->sockets[] = [
            'fp' => $fp,
            'active' => true,
            'result' => null,
        ];

        return true;
    }

    /**
     * Get current task output
     * @return false or array tasks output 
     */
    public function GetTasksOutput($callback = null) 
    {
        $return = false;
        foreach ($this->sockets as $key => &$socket) {
            if($socket['active']){
                $output = $this->GetTaskOutput($socket);
                if($output){ 
                    $socket['result'] = $output;
                    $socket['active'] = false;
                    $return[]  = $output;
                }
            }
        }
        return $return;
    }

    /**
     * @return boolean
     */
    public function HasActiveTasks()
    {
        foreach ($this->sockets as $key => $socket) {
            if ( $socket['active'] )
                return true;
        }
        return false;
    }

    /**
     * Get current task output or false
     * @param array
     * @return False or string output
     */
    private function GetTaskOutput(&$socket)
    {
        if ($socket['fp'] === false) {
            return false;
        }

        if (feof($socket['fp'])) {
            fclose($socket['fp']);
            $socket['fp'] = false;
            $socket['active'] = false;
            return false;
        }

        return fread($socket['fp'], $this->size_output);
    }
}