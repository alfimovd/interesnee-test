<?php

namespace app\controllers;

use yii\filters\VerbFilter;
use app\helpers\MathHelper;

class TaskController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'pi-calc' => ['get','post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->redirect('site/index');
    }

    public function actionPiCalc($dotCount=10000, $size=1000)
    {
    	if(isset($_POST['dotCount'])) {
    		$dotCount = $_POST['dotCount'];
    	}
    	if(isset($_POST['size'])) {
    		$size = $_POST['size'];
    	}
    	$mypi = MathHelper::PiMonteCarlo($dotCount, $size);
        return '<result>'.$mypi.'</result>';
    }

}
