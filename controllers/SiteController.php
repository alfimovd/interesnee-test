<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\helpers\ParallelHelper;
use app\models\LoginForm;
use app\models\ContactForm;
use alfimovd\ParallelComputing\ParallelComputing;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($count_task = 2)
    {
        Yii::setAlias('@alfimovd', '@app/alfimovd');
        $time_start = microtime(true);

        // create object
        $tasks = new ParallelComputing(
            $_SERVER['HTTP_HOST'],
            "/task/pi-calc/"
            );

        // start tasks
        for ($i=0; $i < $count_task; $i++) { 
            $tasks->StartTask([
                    'dotCount' => rand(10000, 1000000),
                    'size' => rand(1000, 10000),
                ]);
        }

        $result = '';
        $pi=0;
        while ( $tasks->HasActiveTasks() ) {
            // delay
            usleep(rand(500000, 100000));
            // get formatted output
            $output = ParallelHelper::FormatOutput($tasks->GetTasksOutput());
            if($output){
                // calc average value
                if($pi){
                    $output[] = $pi;
                }
                $pi = array_sum($output)/count($output);
            }
            // data for view
            $result[] = [
                "time" => microtime(true) - $time_start,
                "pi" => $pi
            ];
        }
        return $this->render('index', [ 
                'result' => $result,
                'count_task' => $count_task,
            ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
