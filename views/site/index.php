<?php

/* @var $this yii\web\View */

$this->title = 'Тестовое задание';
?>
<div class="site-index">
<h1>Таблица значений числа Пи, рассчитанного методом Монте-Карло</h1>
<h2>Количество парралельных процессов <?=$count_task?></h2>
<p>Введите количество потоков для рассчета</p>
<form action="/site/" method="get">
	<div class="form-group">
		<input type="number" name="count_task" >
		<input type="submit" value="Рассчитать"?>
	</div>
</form>
<table class="table .table-hover">
	<tr>
		<th>#</th>
		<th>Время</th>
		<th>Значение pi</th>
	</tr>
    <?php foreach ($result as $key => $value) { ?>
  	<tr>
  		<td>
  			<?=$key?>
  		</td>
  		<td>
  			<?=$value['time']?>
  		</td>
  		<td>
  			<?=$value['pi']?>
  		</td>
  	</tr>
  <?php } ?>
</table>
<h3>Точное значение числа Пи 3.141592653589793238462643...</h3>	
</div>
