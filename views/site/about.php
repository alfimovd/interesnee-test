<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Описание';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="slider" style="padding-bottom: 10px;">
                    <p>
	&nbsp;</p>
	<p style="font-weight: normal; line-height: 1.1em; font-size: 17px; color: rgb(51, 153, 0); font-family: arial, verdana, sans-serif;">
		<span style="font-size:14px;"><strong>Параллельные вычисления</strong></span></p>
	<p>
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Суть задачи:</strong><span style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">&nbsp;Требуется написать класс (семейство классов), позволяющий реализовывать параллельные вычисления на РНР</span><span style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">.</span></p>
	<p>
		&nbsp;</p>
	<p style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-align: justify;">
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Объяснение сути задачи:</strong><br style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">
		<span style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">С помощью данного класса или системы классов становится возможным запускать php скрипты или их части в отдельных процессах. Причём взаимодествие между процессами осуществляется также посредством разработанных классов. При этом под процессом понимается каждый запущеный экземпляр php скрипта.</span></p>
	<p style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-align: justify;">
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Обязательные требования к скрипту:</strong></p>
	<ul>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Весь код должен быть написан в OOП стиле, т.е. с использованием классов, интерфейсов, исключений и т.д.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Весь код должен быть прокомментирован в стиле PHPDocumentor'a.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Скрипт должен функционировать как в Windows так и в Unix системах.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Должна быть возможность распараллеливать не только скрипт целиком, но и конкретную функцию или метод класса.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Скрипт запускающий параллельные процессы должен иметь возможность общаться с ними в обоих направлениях. А именно: отправлять данные в дочерние процессы в любое время, получать данные с любого из дочерних процессов в любое время, определять статус процесса (работает, не работает). Дочерние скрипты также могут отправлять данные в порождающий процесс в произвольное время.</span></li>
	</ul>
	<p style="text-align: justify;">
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Тестовый пример:</strong><br>
		<span style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Испытать ваш класс (классы) для распараллеливания предлагается на следующем примере: вычисление числа pi с заданной точностью методом монте карло (см. википедию). Порождающий скрипт запускает несколько дочерних процессов (количество процессов должно быть параметром конфига), каждый из которых начинает вычислять число pi методом монте карло. При этом при запуске параллельного процесса ему должно быть передано количество итераций вычислений, определяемое случайным образом. Порождающий скрипт через неравные промежутки времени (определяемые так же случайно) должен собирать информацию со всех параллельных процессов и выдавать совокупный результат (значение числа pi) в браузер с указанием времени прошедшей с момента запуска главного скрипта.</span></p>
	<p>
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Что нyжно знать (либо изучить) при выполнении данного теста:</strong></p>
	<ul>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Про PHPDocumentor можно почитать&nbsp;</span><a href="http://manual.phpdoc.org/HTMLSmartyConverter/HandS/phpDocumentor/tutorial_phpDocumentor.pkg.html" style="background-color: transparent; font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-decoration: none; color: rgb(76, 148, 219);" target="_blank">тут</a><span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">;</span></li>
		<li style="text-align: justify;">
			<a href="http://us.php.net/zend-engine-2.php" style="background-color: transparent; font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-decoration: none; color: rgb(76, 148, 219);" target="_blank">ООП в&nbsp;</a><a href="https://kb.clickbrand.com:9100/bin/view/Clickbrand/PHP" style="background-color: transparent; font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-decoration: none; color: rgb(76, 148, 219);">PHP</a><span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">;</span></li>
		<li style="text-align: justify;">
			<a href="http://en.wikipedia.org/wiki/Parallel_computing" style="background-color: transparent; font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-decoration: none; color: rgb(76, 148, 219);" target="_blank">Параллельные вычисления</a></li>
		<li style="text-align: justify;">
			<a href="http://en.wikipedia.org/wiki/Parallel_computing" style="background-color: transparent; font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px; text-decoration: none; color: rgb(76, 148, 219);" target="_blank"><span style="background-color: transparent; color: rgb(0, 0, 0);">А ещё много гуглить и думать...</span></a></li>
	</ul>
	<p style="text-align: justify;">
		&nbsp;</p>
	<p style="text-align: justify;">
		<strong style="color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Как будет оцениваться ваш тест:</strong></p>
	<ul>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Исполняемость и соответствие логики работы скрипта заданию. Если ваш скрипт не реализует распараллеливание процессов, или, в принципе, не может быть исполнен из-за каких-то синтаксических проблем в коде (код либо недописан, либо имеет ошибки синтаксиса), то тестовое задание будет считаться проваленным.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Наличие выполненых обязательных требований к скрипту. Если какое-то из обязательных требований не будет выполнено, то это ещё не значит что вы не пройдёте тест. Однако чем больше обязательных требований будет реализовано тем выше вероятность успеха.</span></li>
		<li style="text-align: justify;">
			<span style="background-color: transparent; color: rgb(0, 0, 0); font-family: arial, verdana, sans-serif; font-size: 13px; line-height: 20.078125px;">Консультация с нами по поводу задания. Мы всегда приветствуем обсуждение задачи перед началом её выполнения, с тем чтобы чётко представлять что нужно сделать. Если у вас есть вопросы, мы всегда с радостью на них ответим.</span></li>
	</ul>
    <br>
    <div class="clear"></div><br>
    </div>
</div>
